

import LoginPage from '../screens/LoginPage';
import FirstPage from '../screens/InitialRoute';
import { createStackNavigator } from 'react-navigation';

export default RootStack extends React.Component{

    render(){
      const RootStack = createStackNavigator({
        Home: {
          screen: FirstPage
        },
      });
      return <RootStack> 
    }
}
