import React, { Component } from 'react';
import {Platform,
   AppRegistry,
   StyleSheet,
   TouchableOpacity,
   Text, ScrollView, Image, StatusBar, SafeAreaView,
   View, Alert, Dimensions,
 } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
import { createStackNavigator } from 'react-navigation';
import ViewPager from 'react-native-view-pager';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import SliderEntry from './SliderEntry';

const IS_IOS = Platform.OS === 'ios';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const slideHeight = 200;
const slideWidth = 600;
const itemHorizontalMargin = wp(35);

export const sliderWidth = 300;
export const itemWidth = 250;
const SLIDER_1_FIRST_ITEM = 0;

export default class CarouselPagerView extends Component {

  constructor (props) {
        super(props);
        this.state = {
            slider1ActiveSlide: SLIDER_1_FIRST_ITEM
        };
    }

  _renderItem ({item, index}) {
         return <SliderEntry data={item} index={index} />;
     }

  render () {
        const { slider1ActiveSlide } = this.state;
        return (
            <SafeAreaView style = {{backgroundColor: '#777777', flex: 1}}>
                <View >
                    <StatusBar
                      translucent={false}
                      backgroundColor='#343434'
                      barStyle={'light-content'}
                    />

                    <ScrollView
                      scrollEventThrottle={200}
                      directionalLockEnabled={true}>

                        <View style={{backgroundColor: 'red', height: 5, width: viewportWidth}}></View>
                        <View style={{backgroundColor: 'yellow', height: 5, width: viewportWidth}}></View>
                        <LinearGradient
                           start={{x: 0.0, y: 0.45}}
                           end={{x: 0.7, y: 1.0}}
                           colors={['#8055d1', '#7452d6', '#585ed9']}
                           style={{
                             paddingTop: 10,
                             paddingBottom: 10,
                             borderBottomLeftRadius: 10,
                             borderBottomRightRadius: 10
                           }}>
                            <View style={{ height: 170, paddingBottom: 8}}>

                              <Carousel
                                ref={c => this._slider1Ref = c}
                                data={ENTRIES1}
                                renderItem={this._renderItem}
                                firstItem = {0}
                                sliderWidth={viewportWidth}
                                itemWidth={viewportWidth - 120}
                                hasParallaxImages={true}
                                inactiveSlideScale={0.70}
                                inactiveSlideOpacity={0.5}
                                loop={false}
                                loopClonesPerSide={2}
                                autoplay={false}
                                autoplayDelay={500}
                                autoplayInterval={3000}
                                onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
                              />
                          </View>
                        </LinearGradient>
                        <Pagination
                          dotsLength={ENTRIES1.length}
                          activeDotIndex={slider1ActiveSlide}
                          containerStyle={styles.paginationContainer}
                          dotColor={'rgba(255, 255, 255, 0.92)'}
                          dotStyle={styles.paginationDot}
                          inactiveDotColor={'#000000'}
                          inactiveDotOpacity={0.4}
                          inactiveDotScale={0.6}
                          carouselRef={this._slider1Ref}
                          tappableDots={!!this._slider1Ref}
                        />
                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}

const ENTRIES1 = [
    {
        title: 'Beautiful and dramatic Antelope Canyon',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://i.imgur.com/UYiroysl.jpg'
    },
    {
        title: 'Earlier this morning, NYC',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://i.imgur.com/UPrs1EWl.jpg'
    },
    {
        title: 'White Pocket Sunset',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
        illustration: 'https://i.imgur.com/MABUbpDl.jpg'
    },
    {
        title: 'Acrocorinth, Greece',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://i.imgur.com/KZsmUi2l.jpg'
    },
    {
        title: 'The lone tree, majestic landscape of New Zealand',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://i.imgur.com/2nCt3Sbl.jpg'
    },
    {
        title: 'Middle Earth, Germany',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://i.imgur.com/lceHsT6l.jpg'
    }
];


const styles = StyleSheet.create({
  paginationContainer: {
        marginTop: 20,
        paddingVertical: 8
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 2
    }
});
