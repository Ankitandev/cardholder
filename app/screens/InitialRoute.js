/**
 * Sample React Native App
 */

 import React, { Component } from 'react'
 import {
   AppRegistry,
   StyleSheet,
   TouchableOpacity,
   Text,
   View, Alert,
 } from 'react-native'


 export default class FirstPage extends Component {

 navigateToCorouselView = () => {
   this.props.navigation.navigate('CarouselPagerView');
 }

 navigateToSimplePagerView = () => {
   this.props.navigation.navigate('SimplePagerView');
 }

 navigateToSocializeHere = () => {
   this.props.navigation.navigate('SocializeHere');
 }


  render() {
    return (
      <View style={styles.container}>

        <Text>Choose the option from below</Text>
        <TouchableOpacity style = {styles.button}
          onPress = {this.navigateToCorouselView}>
          <Text
            style={{fontWeight: 'bold', fontSize: 12}}>
            Go To Corousel View Pager
          </Text>
        </TouchableOpacity>

        <TouchableOpacity style = {styles.button}
          onPress = {this.navigateToSimplePagerView}>
          <Text
            style={{fontWeight: 'bold', fontSize: 12}}>
            Go To Simple View Pager
          </Text>
        </TouchableOpacity>

        <TouchableOpacity style = {styles.button}
          onPress = {this.navigateToSocializeHere}>
          <Text
            style={{fontWeight: 'bold', fontSize: 12}}>
            Go Socialize Bro!
          </Text>
        </TouchableOpacity>

       </View>
     )
   }
 }

 const styles = StyleSheet.create({
   container: {
     flex: 1,
     justifyContent: 'flex-start',
     alignItems: 'center',
     paddingTop: 16,
     paddingRight: 16
   },
   button: {
     alignItems: 'center',
     backgroundColor: '#5dade2',
     padding: 4,
     marginTop: 30
   },
   countContainer: {
     alignItems: 'center',
     padding: 10,
   },
   countText: {
     color: '#FF00FF'
   }
 })

 AppRegistry.registerComponent('App', () => App)
