import React, { Component } from 'react';
import {Platform,
   AppRegistry,
   StyleSheet,
   TouchableOpacity,
   Text, ScrollView, Image,
   View, Alert, Dimensions,
 } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';
import { createStackNavigator } from 'react-navigation';
import ViewPager from 'react-native-view-pager';

const viewportWidth = Dimensions.get('window').width;
export default class SimplePagerView extends Component {

  constructor(props) {
    super(props);

    this._viewPager = null;

    this.state = {
      page: 0,
      pageScroll: '',
      pageSelected: '',
      scrollState: ''
    }
  }

  _renderPage(index) {

    index = index - 1;
    return (
      <View key={index}>
        <TouchableOpacity style = {{width: viewportWidth - 4, height: 200}} onPress = {() => {alert(DATA[index].imageTitle + ' : ' + DATA[index].imageSubtitle)}}>
          <View style = {{padding: 4}}>
            <Image style = {{width: viewportWidth, height: 200}}source={{uri: DATA[index].imageUrl}} resizeMode = 'stretch' />
          </View>
        </TouchableOpacity>
        <Text
          style = {{margin: 4, backgroundColor: 'white', height: '100%', padding: 4, color: '#641E16', fontSize: 12, fontWeight: 'bold'}}>
          {DATA[index].imageTitle + ' : ' + DATA[index].imageSubtitle}
        </Text>
      </View>
    );
  }

  render() {
    var pages = [];
     for(i = 1; i <= DATA.length; i++) {
       pages.push(this._renderPage(i));
     }
    return (
      <View style={{backgroundColor: 'red', flex: 1}}>
            <View style = {{height: 250, width: viewportWidth - 32,}}>
             <ViewPager
                ref={(viewPager) => {this._viewPager = viewPager}}
                style={styles.scrollview}
                initialPage = {3}>
                {pages}
              </ViewPager>
             </View>
          <View style = {{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TouchableOpacity style = {styles.button1}
              onPress = {() => {this._viewPager.setPage(0)}}>
                <Text fontSize = {12}>Move to first</Text>
            </TouchableOpacity>

            <TouchableOpacity style = {styles.button2}
              onPress = {() => {this._viewPager.setPage(DATA.length -1)}}>
                <Text fontSize = {12}>Move to last</Text>
            </TouchableOpacity>
          </View>
       </View>
     )
   }
 }

 const DATA = [
   {
     imageTitle: 'Amsterdam',
     imageSubtitle: 'This place is amazing to have a holiday.',
     imageUrl: 'https://i.imgur.com/UYiroysl.jpg'
   },
   {
     imageTitle: 'Los Angeles',
     imageSubtitle: 'This place is famous for hollywood.',
     imageUrl: 'https://i.imgur.com/UPrs1EWl.jpg'
   },
   {
     imageTitle: 'Las Vegas',
     imageSubtitle: 'This place is famous for gambling.',
     imageUrl: 'https://i.imgur.com/MABUbpDl.jpg'
   },
   {
     imageTitle: 'New York',
     imageSubtitle: 'This place is the land of oportunities.',
     imageUrl: 'https://i.imgur.com/KZsmUi2l.jpg'
   },
   {
     imageTitle: 'Detroit',
     imageSubtitle: 'This place is famous for gangsters in america.',
     imageUrl: 'https://i.imgur.com/2nCt3Sbl.jpg'
   },
   {
     imageTitle: 'Paris',
     imageSubtitle: 'This place is a famous for the eifil tower... hope the spelling is correct.',
     imageUrl: 'https://i.imgur.com/lceHsT6l.jpg'
   },
 ]

 const styles = StyleSheet.create({
  linearGradient: {
    height: 280,
    padding: 16,
    justifyContent: 'space-around',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  scrollview: {
    width: viewportWidth,
    height: 250,
    backgroundColor: 'red',
    flex: 1,
    padding: 8
  },
  button1: {
    alignItems: 'center',
    backgroundColor: '#5dade2',
    padding: 4,
    marginTop: 30,
    width: 120,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10
  },
  button2: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#5dade2',
    padding: 4,
    marginTop: 30,
    width: 120,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10
  },

});
