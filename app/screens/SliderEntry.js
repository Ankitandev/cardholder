import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet, Dimensions} from 'react-native';
import PropTypes from 'prop-types';
import { ParallaxImage } from 'react-native-snap-carousel';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
export default class SliderEntry extends Component {


  constructor (props) {
        super(props);
        this.state = {
            title: '',
            image: '',
            subTitle: ''
        };
    }

    static propTypes = {
        data: PropTypes.object.isRequired,
        index: PropTypes.number,
        parallax: PropTypes.bool,
        parallaxProps: PropTypes.object
    };


componentDidMount(){
    this.setState({
        title: this.props.data.title,
        image: this.props.data.illustration,
        subTitle: this.props.data.subTitle
    })
}

    get image () {
        const { data: { illustration }, index } = this.props;



        return (<Image
              style = {{height: 100, width: 100}}
              source={{ uri: illustration}}
              resizeMode = 'contain'/>
        );
    }

    render () {
      const { data: { title, subtitle }, index } = this.props;
      const itemNumber = index + 1;
        return (
            <TouchableOpacity
              activeOpacity={1}
              style={styles.slideInnerContainer}
              onPress={() => { alert('You tapped on item number ' + itemNumber); }}>
                <View>{ this.image }</View>
                <View>
                    <Text
                      numberOfLines = {1}
                      ellipsizeMode = {'tail'}
                      style = {{fontWeight: 'bold', fontSize: 10}}>
                        {title}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}


const styles = StyleSheet.create({
    slideInnerContainer: {
      backgroundColor: 'white',
      padding: 16,
      borderRadius: 6,
    },
    shadow: {
        position: 'absolute',
        top: 0,
        bottom: 18,
        shadowColor: '#000000',
        shadowOpacity: 0.25,
        shadowOffset: { width: 0, height: 10 },
        shadowRadius: 10,
        borderRadius: 6
    },
});
