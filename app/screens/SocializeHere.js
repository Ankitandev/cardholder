/**
* Done via social network login
*/

import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Alert, Image } from 'react-native';
import { LoginManager, AccessToken } from 'react-native-fbsdk';

export default class SocializeHere extends Component{

  constructor(props){
    super(props);
    this.state = {
      imageUrl: '',
      name: 'Please Login to see name',
      email: 'Please Login to see email',
      isLoggedIn: false
    }
  }


  async loginWithFacebook() {
        try {
            let loginManagerResult = await LoginManager.logInWithReadPermissions(['email', 'public_profile', 'user_photos']);
            if (loginManagerResult.isCancelled)
                console.log('Login was cancelled')

            else {

                let accessTokenData = await AccessToken.getCurrentAccessToken()
                let graphApi = 'https://graph.facebook.com/v2.8/' + accessTokenData.userID + '?fields=name,email,picture.type(large)&access_token=' + accessTokenData.accessToken
                return {graphApiData: await(await fetch(graphApi)).json(), token: accessTokenData}
            }
        } catch (error) {
            alert(error);
            console.log('\n[ERROR] Login with facebook: ${error}\n')
        }
    }

    async startLoginProcess() {
      let facebookUser = await this.loginWithFacebook();
      // alert(JSON.stringify(facebookUser.graphApiData.picture.data.url));
      this.setState({
        imageUrl: facebookUser.graphApiData.picture.data.url,
        name: facebookUser.graphApiData.name,
        email: facebookUser.graphApiData.email,
        isLoggedIn: true
      })
    }

  render(){
    return(
      <View style = {{backgroundColor: 'yellow', flex: 1, padding: 16}}>
        <TouchableOpacity
          onPress={() => this.startLoginProcess()}
          style={{}}>
            <Text>Login with facebook</Text>
        </TouchableOpacity>

        <Image
          style={{width: 150, height: 150, margin: 30, borderRadius: 75}}
          source={{uri: this.state.isLoggedIn ? this.state.imageUrl : 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
        />
        <Text>{this.state.name}</Text>
        <Text>{this.state.email}</Text>
      </View>
    )
  }
}
