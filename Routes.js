import React, { Component } from 'react';

import CarouselPagerView from './app/screens/CarouselPagerView';
import SimplePagerView from './app/screens/SimplePagerView';
import FirstPage from './app/screens/InitialRoute';
import SocializeHere from './app/screens/SocializeHere';
import { createStackNavigator } from 'react-navigation';

export default class Routes extends React.Component {
  render() {
    return <RootStack />;
  }
}
const RootStack = createStackNavigator(
  {
    FirstPage : {screen: FirstPage},
    CarouselPagerView: CarouselPagerView,
    SimplePagerView: SimplePagerView,
    SocializeHere: SocializeHere
  },
  {
    initialRouteName: 'FirstPage',
    headerMode: 'none'
  }
);
