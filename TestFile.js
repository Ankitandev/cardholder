import React, {Component} from 'react';
import {createStackNavigator} from 'react-navigation';
import LoginPage from './app/screens/LoginPage';
import FirstPage from './app/screens/InitialRoute';

export default class Navigator extends React.Component {
    render() {
        const Navigator = createStackNavigator({
                FirstPage: {screen: FirstPage},
                LoginPage: LoginPage,
              },
          {
              initialRouteName: FirstPage,
              headerMode: 'none',
              navigationOptions: {gesturesEnabled: false}   // to prevent back on swipe gesture on IOS.
          });

      return <Navigator/>
  }
}
